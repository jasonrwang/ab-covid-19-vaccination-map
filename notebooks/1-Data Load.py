# %% [markdown] Data Load

# This notebook is meant to be run automatically in a `.py` version once per day.

# %%
import pandas as pd
from bs4 import BeautifulSoup
import requests
import os
import hashlib
import pickle as pkl

# %% [markdown] Data Load

# ## Load Data

# Retrieve data from Alberta.ca

# %%
url = 'https://www.alberta.ca/data/stats/lga-coverage.csv'
req = requests.get(url)
with open('../data/inputs/latest.csv', 'wb') as outfile:
    outfile.write(req.content)

# %% [markdown]

# Check if this is the same data as the version last downloaded.

# %% 

latestHash = hashlib.sha256()
with open('../data/inputs/latest.csv',"rb") as f:
    # Read and update hash string value in blocks of 4K
    for byte_block in iter(lambda: f.read(4096),b""):
        latestHash.update(byte_block)

lastHash = hashlib.sha256()
with open('../data/inputs/last.csv',"rb") as f:
    for byte_block in iter(lambda: f.read(4096),b""):
        lastHash.update(byte_block)

if latestHash.hexdigest() == lastHash.hexdigest():
    print('The latest data is the same as the last saved data.')
    os.remove('../data/inputs/latest.csv')
    quit()
else:
    print('There is new data.')
    df = pd.read_csv('../data/inputs/latest.csv')
    os.rename('../data/inputs/latest.csv','../data/inputs/last.csv')

# %% [markdown]
# Alberta is not timestamping their data, so we need to do that manually and then save that as an appended list.

page_name = 'https://www.alberta.ca/stats/covid-19-alberta-statistics.htm#data-export'
page = requests.get(page_name)

soup = BeautifulSoup(page.text, 'html.parser')
date = pd.to_datetime(
    soup
    .find("div", {'id':'vaccine-data'})
    .find('p').contents[0]
    .strip('Data included up to end of day ').strip('.')
    ).strftime('%Y-%m-%d')
df['Date'] = date

# %% [markdown]
# ## Save Data

# Load the existing data file and append this new data to it.
dfData = pd.read_csv('../data/outputs/data.csv',index_col=0)
dfData = dfData.append(df).reset_index(drop=True)
# Drop duplicate entries
dfData = dfData.drop_duplicates(subset=['Date','Local Code','Age Group'])

# %%
#  Write data to file in multiple formats for easy interoperability
dfData.to_pickle('../data/outputs/data.pkl')
dfData.to_csv('../data/outputs/data.csv')

# %%
