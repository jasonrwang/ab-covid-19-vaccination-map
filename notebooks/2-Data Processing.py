# %% [markdown]
# # Data Processing
#
# We need to pair two datasets together: geographic data and vaccination data. The former is difficult to find from the Government of Alberta open data website, but was republished last year by the [community](https://resources-covid19canada.hub.arcgis.com/datasets/exchange::alberta-health-local-geographic-area/about). The latter is from the previous notebook and saved in `../data/outputs/data.*` as a CSV, JSON, and PKL for interoperability. Note this set was manually pieced together so is missing dates, and we may need to interpolate between those.

# %%
import pandas as pd
import geopandas as gpd

# %% [markdown]
# ## Vaccination Data

# %%
df = pd.read_csv('../data/outputs/data.csv',index_col=0)
df

# %% [markdown]
# We need to filter out all the overlapping age categories, especially since the 12+ and 75+ categories were only added in June. These are useful now, but not in the compiled set.

# %%
df = df.query("`Age Group`!='All age years' & `Age Group`!='ALL years' & `Age Group`!='12+ years'")

# %% [markdown]
# ### Interpolate

# %%
# Not needed?

# %% [markdown]
# The population data for the age range 12-19 changes by 166,581 between May 24 and 25 for some reason in every zone. Based on actual reopening, the newer population seems more accurate. We should then replace the earlier set with the newer.

# %%
newPopulation = df.query("Date=='2021-05-25'").set_index(['Local Code','Age Group'])['Population']

# %%
df = df.drop(columns='Population')
df = df.set_index(['Local Code','Age Group']).join(newPopulation)
df = df.reset_index().sort_values(['Date','Local Code','Age Group'])

# %% [markdown]
# ### Calculate percentage

# %% [markdown]
# #### Local Region

# %%
dfPercent = df.groupby(['Date','Local Code']).sum()

# %% [markdown]
# Drop the misleading data

# %%
dfPercent = dfPercent[['Population','# of population with at least 1 dose','# of population fully immunized']]

# %%
dfPercent['Proportion of eligible population one dose'] = dfPercent['# of population with at least 1 dose']/dfPercent['Population']
dfPercent['adjustedProportion one dose'] = dfPercent['Proportion of eligible population one dose'] - dfPercent.groupby('Local Code')['Proportion of eligible population one dose'].min()
dfPercent = dfPercent.reset_index()

# %% [markdown]
# Check outputs

# %%
dfPercent

# %% [markdown]
# #### Alberta
#
# Also create summary Alberta numbers that will not have a geolocation related to them. These will be merged at the very last step below.

# %%
albertaData = df.groupby(['Date']).sum().reset_index()
albertaData = albertaData[['Date','Population','# of population with at least 1 dose','# of population fully immunized']]
albertaData = albertaData.assign(**{'Zone Name':'Alberta','Local Name':'Alberta'})
albertaData['Proportion of eligible population one dose'] = albertaData['# of population with at least 1 dose']/albertaData['Population']
albertaData['adjustedProportion one dose'] = albertaData['Proportion of eligible population one dose'] - albertaData['Proportion of eligible population one dose'].min()

# %% [markdown]
# ## Geographic Data

# %% [markdown]
# Load shapefile.

# %%
shape_file_loc = '../data/inputs/Alberta_Health_Local_Geographic_Area-shp/356da637-0561-4fc1-a170-e4fd00629e0e2020329-1-f8d8o1.vne9i.shp'

gdf = gpd.read_file(shape_file_loc)

# %% [markdown]
# Rename columns to match our other DataFrame.

# %%
# Needs to match with Shapefile names.
gdf = gdf.rename(columns={'LOCAL_CODE':'Local Code','LOCAL_NAME':'Local Name','ZONE_NAME':'Zone Name'})

# %% [markdown]
# Match with vaccination data

# %%
gdf = gdf.merge(dfPercent,on=['Local Code'])

# %% [markdown]
# Title case zone names for legibility

# %%
gdf['Zone Name'] = gdf['Zone Name'].str.title()
gdf['Local Name'] = gdf['Local Name'].str.title()

# %%
gdf = gdf.append(albertaData)

# %%
gdf.head()

# %%
gdf.tail()

# %% [markdown]
# ## Save Processed Data

# %%
gdf.to_file('../data/outputs/geoVax.geojson', driver='GeoJSON')

# %%
