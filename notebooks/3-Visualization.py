# %% [markdown]
# # Visualization

# %% [markdown]
# ## Import modules

# %%
import altair as alt
import pandas as pd
import geopandas as gpd
import json
import datetime
import imageio
import os

# %% [markdown]
# ## Load Data

# %% [markdown]
# This data should be already partly processed by `2-Data Processing.py`.

# %%
gdf = gpd.read_file('../data/outputs/geoVax.geojson')

# %% [markdown]
# ## Process Data
#
# We need to do a tiny bit of tidying for analysis in this section.

# %% [markdown]
# Change format of date column from string to datetime, since geoJSON liked the former, but the latter may be more consistent for Altair.

# %%
latestDate = gdf['Date'].max()
subtitle = ['Data: Alberta Health | Visualization: @jasonrwang',f"Last Updated: {latestDate}"]

# %%
gdf['Date'] = pd.to_datetime(gdf['Date'])

# %% [markdown]
# ## Visualize
#
# Note: saving to PNG currently suffers from a resolution/scaling issue. `scale_factor` should be set to 2, but is left at 1. See: https://github.com/altair-viz/altair/issues/2191.
# %% [markdown]
# ### Time Series

# %% [markdown]
# #### Zonal
#
# AHS has five main zones in Alberta: North, Central, South, Edmonton, and Calgary.

# %% By Region
chartTotal = alt.Chart(
    gdf.query("`Zone Name`=='Alberta'")
).mark_line(size=3).encode(
    x=alt.X('Date:T',title='Date'),
    y=alt.Y('Proportion of eligible population one dose:Q',title='Proportion (%)'),
)

chartRegion = alt.Chart(
    gdf[['Date','# of population with at least 1 dose','Population','Zone Name']].groupby(['Date','Zone Name']).sum().reset_index()
).mark_line(opacity=0.5).encode(
    x='Date:T',
    y=alt.Y('Proportion:Q',title='Proportion (%)'),
).transform_calculate(
    # Need to calculate for the zone, which we did not do in previous step
    Proportion=alt.datum['# of population with at least 1 dose']/alt.datum['Population']
)

# labels = alt.Chart(gdf).mark_text(
#     align='left', dx=5
# ).encode(
#     x='max(Date):T',
#     y=alt.Y('Proportion of eligible population one dose:Q', aggregate={'argmax': 'Date'}),
# )

chart = alt.layer(chartTotal,chartRegion).encode(
    color=alt.Color('Zone Name:N',
                    sort=alt.EncodingSortField(
                        'Proportion of eligible population one dose',
                        op='max',
                        order='descending')
                   )
).properties(
    title={
        'text':['Proportion of Eligible Albertan Population with at least',
                'One Dose of COVID-19 Vaccine over Time (interpolated)'],
        'subtitle':subtitle
    }
)
chart
# %%
chart.save('../figures/latestTimeSeries.png', scale_factor=1.0)

# %% [markdown]
# #### Local

# %% [markdown]
# ##### Change in Local

# %% This chart needs to be selected and saved manually
selection = alt.selection_multi(fields=['Local Name'], bind='legend')

chart = alt.Chart(
    gdf[['Date','Local Name','adjustedProportion one dose']]
).mark_line().encode(
    x='Date:T',
    y=alt.Y('adjustedProportion one dose:Q',title='Change in Proportion (%)'),
    color=alt.Color(
        'Local Name:N',
        legend=alt.Legend(values=['Alberta'],
        title='Local Area')
    ),
    opacity=alt.condition(selection, alt.value(1), alt.value(0.2)),
    size=alt.condition(selection, alt.value(4), alt.value(1)),
    tooltip='Local Name:N'
).add_selection(
    selection
).properties(
    title={
        'text':['Change in Proportion of Eligible Albertan Population with at least One Dose',
                'of COVID-19 Vaccine over Time in All Local Zones (interpolated)'],
        'subtitle':subtitle
    }
)
chart
# %%
# Best to save this manually at the moment after selecting Alberta
# chart.save('../figures/latestTimeSeriesAllLocal.png')

# %% [markdown]
# ##### Interactive Local

# %%
input_dropdown = alt.binding_select(options=list(gdf.sort_values('Local Name')['Local Name'].unique()))
selection = alt.selection_single(fields=['Local Name'], bind=input_dropdown, name='Selected', empty='none', on='mouseover', nearest=True)

chart = alt.Chart(
    gdf[['Date','Local Name','Proportion of eligible population one dose']]
).mark_line().encode(
    x='Date:T',
    y=alt.Y('Proportion of eligible population one dose:Q',title='Proportion (%)'),
    color=alt.Color(
        'Local Name:N',
        legend=None
    ),
    opacity=alt.condition(selection, alt.value(1), alt.value(0.2)),
    size=alt.condition(selection, alt.value(3), alt.value(0.6)),
    tooltip='Local Name:N'
).add_selection(
    selection
).properties(
    height=400,
    width=800,
    title={
        'text':['Proportion of Eligible Albertan Population with at least One Dose',
                'of COVID-19 Vaccine over Time in All Local Zones (interpolated)'],
        'subtitle':subtitle
    }
)
chart

# %%
chart.save('../figures/interactiveLocal.json')

# %% [markdown]
# ### Map
# We can only load GeoData from one date, so let's take the latest
# 'TypeError: Object of type Timestamp is not JSON serializable' - can perhaps skirt this with changing time to string first
# %%
gdfLatest = gdf.reset_index().groupby('Local Code').max('Date')['index'].values
gdfLatest = gdf.iloc[gdfLatest].drop(columns='Date')

choro_json = json.loads(gdfLatest.to_json())

# %%
choro_data = alt.Data(values=choro_json['features'])

# %% [markdown]
# #### Alberta

# %%
chart = alt.Chart(choro_data).mark_geoshape().encode(
    color=alt.Color(
        "properties['Proportion of eligible population one dose']:Q",
        scale=alt.Scale(scheme='blues'),
        title='Proportion (%)')
).project(
    type='conicEqualArea',
    rotate=[115,-54.5,0],
).properties(
    title={
        'text':['Proportion of Eligible Albertan Population with',
                'at least One Dose of a COVID-19 Vaccine'],
        'subtitle':subtitle
    },
    width=300,
    height=400
)
chart
# %%
chart.save('../figures/latestMap.png',scale_factor=2)

# %% [markdown]
# #### Edmonton

# %%
chart = alt.Chart(choro_data).mark_geoshape().encode(
    color=alt.Color(
        "properties['Proportion of eligible population one dose']:Q",
        scale=alt.Scale(domain=[0.1,0.8],scheme='blues'),
        title='Proportion (%)'),
    tooltip=['localName:N','proportion:Q']
).project(
    type='conicEqualArea',
    rotate=[114.37,-51.03,0],
).properties(
    title={
        'text':['Proportion of Eligible Albertan Population with',
                'at least One Dose of a COVID-19 Vaccine (Edmonton)'],
        'subtitle':subtitle},
    width=300,
    height=400
).transform_filter(
    "split(datum.properties['Local Name'],' ',1) == 'Edmonton'"
).transform_calculate(
    # Just to rename things
    proportion = "round(datum.properties['Proportion of eligible population one dose'] * 100)",
    localName = "datum.properties['Local Name']"
)
chart

# %%
chart.save('../figures/latestMapEdmonton.png',scale_factor=2)

# %% [markdown]
# #### Calgary

# %%
chart = alt.Chart(choro_data).mark_geoshape().encode(
    color=alt.Color(
        "properties['Proportion of eligible population one dose']:Q",
        scale=alt.Scale(domain=[0.1,0.8],scheme='blues'),
        title='Proportion (%)'),
    tooltip=['localName:N','proportion:Q']
).project(
    type='conicEqualArea',
    rotate=[114.37,-51.03,0],
).properties(
    title={
        'text':['Proportion of Eligible Albertan Population with',
                'at least One Dose of a COVID-19 Vaccine (Calgary)'],
        'subtitle':subtitle},
    width=300,
    height=400
).transform_filter(
    "split(datum.properties['Local Name'],' ',1) == 'Calgary'"
).transform_calculate(
    # Just to rename things
    proportion = "round(datum.properties['Proportion of eligible population one dose'] * 100)",
    localName = "datum.properties['Local Name']"
)
chart

# %%
chart.save('../figures/latestMapCalgary.png')

# %% [markdown]
# ### Dynamic Map

# %% [markdown]
# This section creates a map per day of day in a `for` loop.
#
# Here, we need to set a consistent colour range. We can find the min and max of the proportions over time and take a heuristical approach to smooth things over.

# %%
(gdf['# of population with at least 1 dose']/gdf['Population']).describe()

# %%
for date in gdf['Date'].unique():
    gdfOne = gdf.query("Date==@date").drop(columns='Date')

    choro_json = json.loads(gdfOne.to_json())
    choro_data = alt.Data(values=choro_json['features'])

    alt.Chart(choro_data).mark_geoshape().encode(
        color=alt.Color('Proportion:Q',scale=alt.Scale(scheme='blues',domain=[0.1,0.8]),title='Proportion (%)')
    ).transform_calculate(
        Proportion="datum.properties['# of population with at least 1 dose']/datum.properties['Population']"
    ).project(
        type='conicEqualArea',
        rotate=[115,-54.5,0],
    ).properties(
        title={
            'text':['Proportion of Eligible Albertan Population with','at least One Dose of a COVID-19 Vaccine'],
            'subtitle':['Data: Alberta Health; Visualization: @jasonrwang',f"Date: {date.strftime('%Y-%m-%d')}"]},
        width=300,
        height=400
    ).save(f'../figures/animatedMap/map_{date}.png')

# %% [markdown]
# Finally, we need to pair the individual pictures together into a GIF.

# %%
animationDir = '../figures/animatedMap/'
images = []
for file_name in sorted(os.listdir(animationDir)):
    if file_name.endswith('.png'):
        file_path = os.path.join(animationDir, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave(animationDir+'ABVaxMapAnimated.gif', images)

# %%
