# %% [markdown]

# # Manual Data Processing

# Since I was not able to put this all together when the vaccine data was first announced, I needed to manually download data when I remembered to. That means we must manually piece together data from these dates.

# This file reads through all the piecemeal data files 

# %%
import pandas as pd
import os

# %% [markdown]
# Get list of all files in the manual folder
filepath = '../data/inputs/manual/'
files = os.listdir(filepath)
df = pd.DataFrame()

# %% [markdown]
# Load all files, asking user for input for the date
for file in files:
    print(file)
    date = input("Input the date ('%Y-%m-%d' format preferred, but something like 'June 1, 2021' also works.).")
    dfFile = pd.read_csv(filepath+file)
    dfFile['Date'] = pd.to_datetime(date).strftime('%Y-%m-%d')
    df = df.append(dfFile)

# %% [markdown]
# Check the data
df
# %% [markdown]
# Save the data
df.to_pickle('../data/outputs/data.pkl')
df.to_csv('../data/outputs/data.csv')
df.to_json('../data/outputs/data.json')
# %%
