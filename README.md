# Alberta COVID-19 Vaccination Map

A live-updating visualization of Alberta's COVID-19 progress that is better than the Government site.

This project is run through VS Code's interactive Python and will be converted into a Jupytext `.md` format periodically (good for CI to do).

Data in inputs and raw, and those called `outputs/data.*` are intermediate files with only formatting changes and no filtering.
